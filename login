import React from "react";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import { httpClient } from "./../../../utils/httpClient";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Profile
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      data: {
        username: "",
        password: "",
      },
      error: {
        username: "",
        password: "",
      },
      isSubmitting: false,
      validForm: true,
    };
  }

  handleChange(e) {
    const { name, value } = e.target;

    this.setState(
      (prevState) => ({
        data: {
          ...prevState.data,
          [name]: value,
        },
      }),
      () => {
        this.validateForm(name);
      }
    );
  }

  validateForm(fieldName) {
    let errMsg;
    errMsg = this.state.data[fieldName] 
      ? "" 
      : `${fieldName} is required`;

    this.setState((prevState) => ({
      error: {
        ...prevState.error,
        [fieldName]: errMsg,
      },
    }));
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({
      isSubmitting: true,
    });
    httpClient.post('/auth/login',this.state.data)
      .then(response=>{
        console.log('data is>>',response);
        localStorage.setItem('token',response.data.token);
        localStorage.setItem('user',JSON.stringify(response.data.user));
        this.props.history.push('/dashboard');
      })
      .catch(err=>{
        console.log('http err >>',err);
        // console.log('err >>',err.response);

        this.setState({
          isSubmitting:false
        })
      })
  };

  render() {
    return (
      <Container component="main" maxWidth="xs">
        <div>
          <Typography component="h1" variant="h5" align="center">
            Sign in
          </Typography>
          <form onSubmit={this.handleSubmit} noValidate={true}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="user"
              label="User Name"
              name="user"
              autoComplete="user"
              autoFocus
              onChange={this.handleChange.bind(this)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={this.handleChange.bind(this)}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button type="submit" fullWidth variant="outlined" color="primary">
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="/forgot-password" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="/register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={8}>
          <Copyright />
        </Box>
      </Container>
    );
  }
}
export default Login;
